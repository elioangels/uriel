#!/usr/bin/env node
import { Command } from "commander"
import path from "path"
import sharp from "sharp"

import app from "../src/index.js"

const program = new Command()

program
  .version("1.0.0", "-v, --version")
  .usage("[OPTIONS]...")
  .option(
    "-t, --thing <str>",
    "The path to the configuration of an artwork thing.",
    "",
  )
  .option("-d, --debug", "Export all images separately", false)
  .parse(process.argv)

const { thing, debug } = program.opts()

const scriptPath = path.resolve(thing)
const scriptRoot = path.resolve(path.dirname(scriptPath))

let { turds } = app({
  debug: debug,
  fontfile: `${scriptRoot}/Montserrat-ExtraBold.ttf`,
  scriptRoot: scriptRoot,
})

let { sharpEngageT } = turds

async function dynamicImport(modulePath) {
  try {
    const { artworkThing } = await import(modulePath)
    sharpEngageT(artworkThing).then(out => {
      let outName = `${scriptRoot}/${artworkThing.url}`
      sharp(out)
        .png()
        .toFile(`${scriptRoot}/${artworkThing.url}`)
        .then(info => console.log({ outName, ...info }))
        .catch(err => console.error({ err }))
    })
  } catch (error) {
    console.error("Artwork thing not found", error)
  }
}

dynamicImport(scriptPath)
