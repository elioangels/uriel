import { ItemListT } from "./ItemListT.js"

export const VisualArtworkT = ({
  identifier,
  artworkSurface,
  height,
  position,
  width,
  url,
  ItemList,
}) =>
  new Object({
    identifier,
    mainEntityOfPage: "VisualArtwork",
    url,
    CreativeWork: {
      position: position || "center",
    },
    VisualArtwork: {
      artworkSurface: artworkSurface || { r: 0, g: 0, b: 0, alpha: 0 },
      height,
      width,
    },
    ItemList: ItemListT(ItemList?.itemListElement),
  })
