export const ItemListT = itemListElement =>
  new Object({
    numberOfItems: itemListElement ? itemListElement.length : 0,
    itemListElement: itemListElement ? itemListElement : [],
  })
