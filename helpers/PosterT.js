import { ItemListT } from "./ItemListT.js"

export const PosterT = ({ identifier, position, text, size, url, ItemList }) =>
  new Object({
    identifier,
    mainEntityOfPage: "Poster",
    url,
    CreativeWork: {
      position,
      text,
      size,
    },
    ItemList: ItemListT(ItemList?.itemListElement),
  })
