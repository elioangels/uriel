export const resizeDrawActionT = (height, width) =>
  new Object({
    mainEntityOfPage: "DrawAction",
    Action: {
      instrument: "resize",
      object: { height, width },
    },
  })

export const invertDrawActionT = () =>
  new Object({
    mainEntityOfPage: "DrawAction",
    Action: {
      instrument: "negate",
      object: { alpha: false },
    },
  })

export const rotateDrawActionT = angle =>
  new Object({
    mainEntityOfPage: "DrawAction",
    Action: {
      instrument: "rotate",
      object: angle,
    },
  })
