import { ItemListT } from "./ItemListT.js"

export const ImageObjectT = ({ identifier, image, position, url, ItemList }) =>
  new Object({
    identifier,
    mainEntityOfPage: "ImageObject",
    image,
    url,
    CreativeWork: {
      position,
    },
    ItemList: ItemListT(ItemList?.itemListElement),
  })
