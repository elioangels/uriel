import sharp from "sharp"
import fs from "fs"
import path from "path"

export const urielArt = appConfig => {
  let { debug, fontfile, scriptRoot } = appConfig

  /** Divide our code into: */
  // turds ("engage Thing" endpoints)
  let turds = {}
  // lutes ("list Thing" endpoints)
  let lutes = {}
  // spine ("util Thing" endpoints)
  let spine = {}

  /** Util to determine where the artwork should be placed within the parent.
   * Defaults: "center". */
  spine.gravityOfT = thing =>
    thing.CreativeWork.position ? thing.CreativeWork.position : "center"

  /** Turd: a **sharp** image layer of the engagedThing.  */
  turds.sharpImageT = engagedThing =>
    sharp(`${scriptRoot}/${engagedThing.image}`)

  /** Turd: a **sharp** colored shapes or a canvas "bg" layer of the engagedThing. */
  turds.sharpDrawingT = engagedThing =>
    sharp({
      create: {
        background: engagedThing.VisualArtwork?.artworkSurface || {
          r: 0,
          b: 0,
          g: 0,
          alpha: 0,
        },
        channels: 4,
        height: engagedThing.VisualArtwork?.height || 568,
        width: engagedThing.VisualArtwork?.width || 568,
      },
    })

  /** Turd: a layer of **sharp** text. */
  turds.sharpTextT = engagedThing =>
    sharp({
      text: {
        font: "Montserrat",
        fontfile: fontfile,
        // height: engagedThing.VisualArtwork.height,
        dpi: engagedThing.CreativeWork.size,
        rgba: true,
        text: engagedThing.CreativeWork.text,
        // width: engagedThing.VisualArtwork.width,
        wrap: "word",
      },
    })

  /** A "hub" for strictly "thing=>sharpThing" endpoints */
  turds.getSharp = {
    ImageObject: turds.sharpImageT,
    VisualArtwork: turds.sharpDrawingT,
    Poster: turds.sharpTextT,
  }

  /** Get a single **sharp** object which can be a layer in the composite, or the object compositing. */
  turds.getPipe = engagedThing => {
    let getPipePromise = (resolve, reject) => {
      let pipe = turds.getSharp[engagedThing.mainEntityOfPage](engagedThing)
      if (!Array.isArray(engagedThing.ItemList?.itemListElement)) {
        engagedThing.ItemList = { itemListElement: [] }
      }
      // Perform **sharp** `DrawAction`s.
      engagedThing.ItemList.itemListElement
        .filter(
          itemListDrawAction =>
            itemListDrawAction.mainEntityOfPage === "DrawAction",
        )
        .forEach(itemListDrawAction => {
          pipe[itemListDrawAction.Action.instrument](
            itemListDrawAction.Action.object,
          )
        })
      // /** @debug Take a copy and save it. */
      if (engagedThing.url && debug) {
        let clonePath = `${scriptRoot}/${engagedThing.url}`
        pipe
          .clone()
          .png()
          .toFile(`${scriptRoot}/${engagedThing.url}`)
          .then(info =>
            console.log("Saved:", {
              fileName: clonePath,
              info,
            }),
          )
      }
      // Pipe artwork as a `png` to buffer.
      pipe.png().toBuffer().then(resolve).catch(reject)
    }
    return new Promise(getPipePromise)
  }

  /** Composite each ItemList onto the engaged `thing` **sharp** layer. */
  lutes.sharpListT = engagedThing => engagedThingBuffer => {
    // Map the itemList into promises which resolve when
    if (!Array.isArray(engagedThing.ItemList?.itemListElement)) {
      engagedThing.ItemList = { itemListElement: [] }
    }
    // each artwork has been created by **sharp**.
    let itemListArtworkPromises = engagedThing.ItemList.itemListElement
      // Do we have a "sharpTurdT" which can handle this Schema type?
      .filter(itemListThing =>
        Object.keys(turds.getSharp).includes(itemListThing.mainEntityOfPage),
      )
      // Promise a **sharp** image buffer for each ItemList Thing.
      .map(
        itemListThing =>
          new Promise((resolve, reject) => {
            // resolve created artwork.
            turds
              .sharpEngageT(itemListThing)
              .then(itemListBuffer =>
                // Resolve artwork buffer to an array of `composite` inputs.
                resolve({
                  input: itemListBuffer,
                  gravity: itemListThing.CreativeWork.position,
                }),
              )
              .catch(reject)
          }),
      )
    // Wait until all the itemList artwork has been created by **sharp**.
    return Promise.all(itemListArtworkPromises).then(
      // Run single `composite` call with all the itemList as `composite` inputs.
      itemListInputs =>
        sharp(engagedThingBuffer).composite(itemListInputs).png().toBuffer(),
    )
  }

  /** Engage a `thing` and create a **sharp** object.  */
  turds.sharpEngageT = engagedThing => {
    return (
      turds
        .getPipe(engagedThing)
        // Pipe into `sharpListT` function.
        .then(lutes.sharpListT(engagedThing))
    )
  }

  return { lutes, turds, spine, appConfig }
}

export default urielArt
