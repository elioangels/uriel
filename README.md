![](https://elioway.gitlab.io/elioangels/uriel/elio-uriel-logo.png)

> Uriel, **the elioWay**

# uriel

Starter pack for an **elioWay** app using `yo thing`(<https://www.npmjs.com/package/generator-thing>)

- [uriel Documentation](https://elioway.gitlab.io/elioangels/uriel/)

## Prerequisites

- [uriel Prerequisites](https://elioway.gitlab.io/elioangels/uriel/installing.html)

## Installing

- [Installing uriel](https://elioway.gitlab.io/elioangels/uriel/installing.html)

## Seeing is Believing

```
You're seeing it.
```

- [elioangels Quickstart](https://elioway.gitlab.io/elioangels/quickstart.html)
- [uriel Quickstart](https://elioway.gitlab.io/elioangels/uriel/quickstart.html)

# Credits

- [uriel Credits](https://elioway.gitlab.io/elioangels/uriel/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioangels/uriel/apple-touch-icon.png)
