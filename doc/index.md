# uriel

<aside>
  <dl>
    <dd>Thither came Uriel, gliding through the even</dd>
    <dd>On a sun-beam, swift as a shooting star</dd>
    <dd>In autumn thwarts the night, when vapours fired</dd>
    <dd>Impress the air, and shows the mariner</dd>
    <dd>From what point of his compass to beware</dd>
    <dd>Impetuous winds: He thus began in haste.</dd>
  </dl>
</aside>

> Uriel, **the elioWay**

![experimental](/eliosin/icon/devops/experimental/favicon.ico "experimental")

## WTF

uriel the **elioWay**
