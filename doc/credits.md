# uriel Credits

## Core Thanks!

- [elioWay](https://elioway.gitlab.io)
- [Yeoman](http://yeoman.io/)

## Artwork

- [Tribal-wings-tattoo-vector-image](https://publicdomainvectors.org/en/free-clipart/Tribal-wings-tattoo-vector-image/29027.html)

## Paradise Lost

```
Thither came Uriel, gliding through the even
On a sun-beam, swift as a shooting star
In autumn thwarts the night
```
