const scale = 1
import {
  ItemListT,
  PosterT,
  ImageObjectT,
  VisualArtworkT,
  resizeDrawActionT,
  invertDrawActionT,
  rotateDrawActionT,
} from "../helpers/index.js"

export const artworkThing = VisualArtworkT({
  height: 558 * scale,
  identifier: "mainIcon",
  url: "../star.png",
  width: 558 * scale,
  ItemList: ItemListT([
    ImageObjectT({
      identifier: "starBlackBorder",
      image: "../../../eliosin/icon/star72pt/artwork/icon.svg",
      position: "center",
      ItemList: ItemListT([resizeDrawActionT(557 * scale, 557 * scale)]),
    }),
    ImageObjectT({
      identifier: "starWhiteHalo",
      image: "../../../eliosin/icon/star72pt/artwork/icon.svg",
      position: "center",
      ItemList: ItemListT([
        resizeDrawActionT(555 * scale, 555 * scale),
        invertDrawActionT(),
      ]),
    }),
    ImageObjectT({
      identifier: "starBlack",
      image: "../../../eliosin/icon/star72pt/artwork/icon.svg",
      position: "center",
      ItemList: ItemListT([
        resizeDrawActionT(515 * scale, 515 * scale),
        rotateDrawActionT(90),
      ]),
    }),
    VisualArtworkT({
      height: 400 * scale,
      identifier: "paddingForVisual",
      position: "center",
      width: 400 * scale,
      ItemList: ItemListT([
        ImageObjectT({
          identifier: "iconVisual",
          image: "./icon.svg",
          position: "south",
          ItemList: ItemListT([
            resizeDrawActionT(370, 370),
            invertDrawActionT(),
          ]),
        }),
      ]),
    }),
  ]),
})

export default artworkThing
