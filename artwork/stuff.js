const scale = 1
import {
  resizeDrawAction,
  invertDrawAction,
  rotateDrawAction,
  ImageObject,
  canvasVisualArtwork,
} from "../helpers/index.js"

canvasVisualArtwork({
  identifier: "mainIcon",
  height: 558 * scale,
  width: 558 * scale,
  url: "../icon.png",
  itemListElement: [
    ImageObject({
      identifier: "starBlackBorder",
      image: "../../../eliosin/icon/star72pt/artwork/icon.svg",
      position: "center",
      itemListElement: [resizeDrawAction(557 * scale, 557 * scale)],
    }),
    {
      identifier: "starBlackBorder",
      mainEntityOfPage: "ImageObject",
      image: "../../../eliosin/icon/star72pt/artwork/icon.svg",
      CreativeWork: {
        position: "center",
      },
      ItemList: {
        itemListElement: [resizeDrawAction(557 * scale, 557 * scale)],
      },
    },
    {
      identifier: "starWhiteHalo",
      mainEntityOfPage: "ImageObject",
      image: "../../../eliosin/icon/star72pt/artwork/icon.svg",
      CreativeWork: {
        position: "center",
      },
      ItemList: {
        itemListElement: [
          resizeDrawAction(555 * scale, 555 * scale),
          invertDrawAction(),
        ],
      },
    },
    {
      identifier: "starBlack",
      mainEntityOfPage: "ImageObject",
      image: "../../../eliosin/icon/star72pt/artwork/icon.svg",
      CreativeWork: {
        position: "center",
      },
      ItemList: {
        itemListElement: [
          resizeDrawAction(515 * scale, 515 * scale),
          rotateDrawAction(90),
        ],
      },
    },
    {
      identifier: "paddingForVisual",
      mainEntityOfPage: "VisualArtwork",
      CreativeWork: {
        position: "center",
      },
      VisualArtwork: {
        artworkSurface: { r: 0, g: 0, b: 0, alpha: 0 },
        height: 200,
        width: 430,
      },
      ItemList: {
        itemListElement: [
          // {
          //   identifier: "iconVisual",
          //   mainEntityOfPage: "ImageObject",
          //   image: "./icon.svg",
          //   CreativeWork: {
          //     position: "south",
          //   },
          //   ItemList: {
          //     itemListElement: [
          //       resizeDrawAction(370, 370),
          //       invertDrawAction(),
          //     ],
          //   },
          // },
          {
            mainEntityOfPage: "Poster",
            CreativeWork: {
              position: "north",
              text: `<span font_weight="bold">elio</span>`,
            },
            VisualArtwork: {
              height: 400,
              width: 400,
            },
            ItemList: {
              itemListElement: [invertDrawAction()],
            },
          },
        ],
      },
    },
  ],
})
export const icon = {
  mainEntityOfPage: "VisualArtwork",
  VisualArtwork: {
    artworkSurface: { r: 128, g: 0, b: 0, alpha: 0 },
    height: 558 * scale,
    width: 558 * scale,
  },
  url: "../icon.png",
  ItemList: {
    itemListElement: [
      {
        identifier: "starBlackBorder",
        mainEntityOfPage: "ImageObject",
        image: "../../../eliosin/icon/star72pt/artwork/icon.svg",
        CreativeWork: {
          position: "center",
        },
        ItemList: {
          itemListElement: [resizeDrawAction(557 * scale, 557 * scale)],
        },
      },
      {
        identifier: "starWhiteHalo",
        mainEntityOfPage: "ImageObject",
        image: "../../../eliosin/icon/star72pt/artwork/icon.svg",
        CreativeWork: {
          position: "center",
        },
        ItemList: {
          itemListElement: [
            resizeDrawAction(555 * scale, 555 * scale),
            invertDrawAction(),
          ],
        },
      },
      {
        identifier: "starBlack",
        mainEntityOfPage: "ImageObject",
        image: "../../../eliosin/icon/star72pt/artwork/icon.svg",
        CreativeWork: {
          position: "center",
        },
        ItemList: {
          itemListElement: [
            resizeDrawAction(515 * scale, 515 * scale),
            rotateDrawAction(90),
          ],
        },
      },
      {
        identifier: "paddingForVisual",
        mainEntityOfPage: "VisualArtwork",
        CreativeWork: {
          position: "center",
        },
        VisualArtwork: {
          artworkSurface: { r: 0, g: 0, b: 0, alpha: 0 },
          height: 200,
          width: 430,
        },
        ItemList: {
          itemListElement: [
            // {
            //   identifier: "iconVisual",
            //   mainEntityOfPage: "ImageObject",
            //   image: "./icon.svg",
            //   CreativeWork: {
            //     position: "south",
            //   },
            //   ItemList: {
            //     itemListElement: [
            //       resizeDrawAction(370, 370),
            //       invertDrawAction(),
            //     ],
            //   },
            // },
            {
              mainEntityOfPage: "Poster",
              CreativeWork: {
                position: "north",
                text: `<span font_weight="bold">elio</span>`,
              },
              VisualArtwork: {
                height: 400,
                width: 400,
              },
              ItemList: {
                itemListElement: [invertDrawAction()],
              },
            },
          ],
        },
      },
    ],
  },
}
